name             'nsd'
maintainer       'GSI HPC Department'
maintainer_email 'c.huhn@gsi.de'
license          'GNU Public License 3.0'
description      'Installs/Configures nsd'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
